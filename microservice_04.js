// Warning ::: cron_restart doesn't work with non-js code
// https://github.com/Unitech/pm2/issues/3616

// This module will be run from bash script
// which in its turn will be run from PM2
// with option cron_reset

const fn = require('./fn');

fn('Microservice №01 ::: Log');

// This line is for keeping nodejs script running
// NOTE ::: PM2 restarts script if it crashes or ends peacefully
// So, if we will remove this line, the script will be end peacefully
// which leads to the restart of it by PM2
setInterval(_ => {}, 1 << 30);
