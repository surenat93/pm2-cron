// This module will be run from PM2 only one time
// And the periodically call of 'fn' function
// will be implemented by usual 'setInterval'
// inside this module

const fn = require('./fn');

fn('Microservice №01 ::: Log');

setInterval(_ => fn('Microservice №01 ::: Log'), 5000);
