// This module will be run from PM2
// using cron_restart option
// which will restart this module
// by the given time interval
// NOTE ::: Notice, that module will be destroyed
// and will be restarted every time,
// which can lead to huge usage of CPU

const fn = require('./fn');

fn('Microservice №02 ::: Log');

// This line is for keeping nodejs script running
// NOTE ::: PM2 restarts script if it crashes or ends peacefully
// So, if we will remove this line, the script will be end peacefully
// which leads to the restart of it by PM2
setInterval(_ => {}, 1 << 30);
