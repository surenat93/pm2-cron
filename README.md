#### Synopsys

This simple app is about different versions of integrations of PM2 process management tool and periodically running scripts.

##### Install

```bash
npm i
```

#### Run

```bash
pm2 start ecosystem.config.json && pm2 monit
```

#### Additional information

At first there are two different cron packages for node

1) cron (more popular)

  - Package name - [cron](https://www.npmjs.com/package/cron)
  - Repo name - [node-cron](https://github.com/kelektiv/node-cron#readme)

2) node-cron

  - Package name - [node-cron](https://www.npmjs.com/package/node-cron)
  - Repo name - [node-cron](https://github.com/merencia/node-cron)

Both of them are using recursive setTimeout for scheduling.

  * [cron](https://github.com/kelektiv/node-cron/blob/master/lib/cron.js#L531)

  * [node-cron](https://github.com/merencia/node-cron/blob/master/src/scheduled-task.js#L22)

[PM2](https://github.com/Unitech/pm2) uses [cron]((https://www.npmjs.com/package/cron)) package for cron jobs.

1) https://github.com/Unitech/pm2/blob/master/package.json#L170

2) https://github.com/Unitech/pm2/blob/master/lib/Common.js#L122

3) https://github.com/Unitech/pm2/blob/master/lib/Common.js#L306
