// This module will be run from PM2 only one time.
// And the periodically call of 'fn' function
// will be implemented by 'cron' package.

const fn = require('./fn');
const { CronJob } = require('cron');

fn('Microservice №03 ::: Log');

new CronJob('*/5 * * * * *', _ => fn('Microservice №03 ::: Log'), null, true);
